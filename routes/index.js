module.exports = function(app, addon) {
    app.get('/healthcheck', function(req, res) {
        res.send('ok');
    });

    app.get('/', function(req, res) {
        res.format({
            'text/html': () => {
                res.redirect('/atlassian-connect.json');
            },
            'application/json': () => {
                res.redirect('/atlassian-connect.json');
            }
        });
    });

    app.get('/web-components', addon.authenticate(), function(req, res) {
        res.render('web-components', {
            title: 'Confluence Web Components',
            xdm_e: req.query.xdm_e,
            cp: req.query.cp
        });
    });

    app.get('/example-site', addon.authenticate(), function(req, res) {
        res.render('example-site', {
            title: 'Confluence Web Components - Example Site',
            xdm_e: req.query.xdm_e,
            cp: req.query.cp
        });
    });

    app.get('/components/:componentName', addon.authenticate(), function(req, res) {
        res.render(`components/${req.params.componentName}`, {
            xdm_e: req.query.xdm_e,
            cp: req.query.cp
        });
    });

    app.get('/themes/:theme/:template', addon.authenticate(), function(req, res) {
        res.render(`themes/${req.params.theme}/${req.params.template}`, {
            contentType: req.query.contentType,
            pageId: req.query.pageId,
            spaceKey: req.query.spaceKey,
            xdm_e: req.query.xdm_e,
            cp: req.query.cp
        });
    });

    app.get('/twenty-fifteen', addon.authenticate(), function(req, res) {
        res.render('twenty-fifteen', {
            title: 'Confluence - Twenty Fifteen Theme',
            xdm_e: req.query.xdm_e
        });
    });

    // load any additional files you have in routes and apply those to the app
    {
        const fs = require('fs');
        const path = require('path');
        const files = fs.readdirSync('routes');
        for(const index in files) {
            const file = files[index];
            if (file === 'index.js') continue;
            // skip non-javascript files
            if (path.extname(file) != '.js') continue;

            const routes = require('./' + path.basename(file));

            if (typeof routes === 'function') {
                routes(app, addon);
            }
        }
    }
};
