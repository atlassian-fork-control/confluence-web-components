const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const errorHandler = require('errorhandler');
const morgan = require('morgan');
const ac = require('atlassian-connect-express');
const hbs = require('express-hbs');
const http = require('http');
const os = require('os');

const viewsDir = __dirname + '/views';

const routes = require('./routes');

const app = express();
const addon = ac(app);
const port = addon.config.port();

const devEnv = app.get('env') == 'development';

app.set('port', port);
app.engine('hbs', hbs.express3({partialsDir: viewsDir}));
app.set('view engine', 'hbs');
app.set('views', viewsDir);

app.use(morgan(devEnv ? 'dev' : 'combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(compression());
app.use(addon.middleware());
hbs.registerHelper('furl', function(url){ return app.locals.furl(url); });
app.use('/dist', express.static(__dirname + '/dist'));
app.use('/public', express.static(__dirname + '/public'));

if (devEnv) app.use(errorHandler());

routes(app, addon);

http.createServer(app).listen(port, function(){
  console.log('Add-on server running at http://' + os.hostname() + ':' + port);
  // Enables auto registration/de-registration of add-ons into a host in dev mode
  if (devEnv) addon.register();
});
