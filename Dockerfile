FROM mhart/alpine-node:4.2.3
MAINTAINER Drew Walker <dwalker@atlassian.com>

RUN mkdir /confluence-web-components
WORKDIR /confluence-web-components

COPY package.json /confluence-web-components
RUN npm install

COPY . /confluence-web-components

EXPOSE 8080
ENV PORT 8080
ENV NODE_ENV production
CMD [ "npm", "start" ]
