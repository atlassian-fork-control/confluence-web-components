plan(key:'WEBCOMPONENTS',name:'confluence-web-components') {
   project(key:'CONFNODE',name:'Confluence Node Modules')

   repository(name:'confluence-web-components')

   stage(name:'Default Stage') {
      job(key:'JOB1',name:'Default Job') {
         artifactDefinition(name:'service-descriptor',pattern:'*.sd.yml',shared:'true')
         task(type:'checkout',description:'Checkout Default Repository') {
            repository(name:'confluence-web-components')
         }
         task(
            type:'script',
            description:'./bamboo-build.sh',
            scriptBody:'./bamboo-build.sh'
         )
         task(
            type:'injectBambooVariables',
            description:'bamboo.properties',
            namespace:'inject',
            scope:'RESULT',
            filePath:'bamboo.properties'
         )
         task(type:'npm',description:'install',command:'install',
            executable:'Node.js 4.2')

         task(type:'npm',description:'run lint',command:'run lint',
            executable:'Node.js 4.2')

      }
   }
   branchMonitoring() {
      createBranch(matchingPattern:'.*')

      inactiveBranchCleanup(periodInDays:'7')

      deletedBranchCleanup(periodInDays:'2')

   }
   dependencies(triggerForBranches:'true')

   permissions() {
      user(name:'dwalker',permissions:'read,write,build,clone,administration')

      anonymous(permissions:'read')

      loggedInUser(permissions:'read')

   }
}

deployment(name:'Deploy confluence-web-components',planKey:'CONFNODE-WEBCOMPONENTS') {
   versioning(version:'${bamboo.inject.packageVersion}-confluence-web-components')

   environment(name:'Domain Development') {
      notification(type:'Deployment Finished',recipient:'hipchat',apiKey:'${bamboo.atlassian.hipchat.apikey.password}',notify:'false',room:'Confluence Frontend')
      trigger(type:'afterSuccessfulPlan')

      task(type:'addRequirement',description:'os equals linux') {
         requirement(key:'os',condition:'equals',value:'Linux')
      }
      task(type:'artifactDownload',description:'service-descriptor',
         planKey:'CONFNODE-WEBCOMPONENTS') {
         artifact(name:'service-descriptor',localPath:'service')
      }
      task(type:'npm',description:'install @atlassian/micros-cli --production',
         command:'install @atlassian/micros-cli --production',
         executable:'Node.js')

      task(type:'script',description:'micros service:deploy',
         scriptBody:'MICROS_TOKEN="$bamboo_micros_webcomponents_token_password" "$(npm bin)/micros" service:deploy "confluence-web-components" -f service/confluence-web-components.sd.yml -e "ddev" -v -u cfn-base')
   }

   environment(name:'Application Development') {
      notification(type:'Deployment Finished',recipient:'hipchat',apiKey:'${bamboo.atlassian.hipchat.apikey.password}',notify:'false',room:'Confluence Frontend')
      trigger(type:'afterSuccessfulPlan')

      task(type:'addRequirement',description:'os equals linux') {
         requirement(key:'os',condition:'equals',value:'Linux')
      }
      task(type:'artifactDownload',description:'service-descriptor',
         planKey:'CONFNODE-WEBCOMPONENTS') {
         artifact(name:'service-descriptor',localPath:'service')
      }
      task(type:'npm',description:'install @atlassian/micros-cli --production',
         command:'install @atlassian/micros-cli --production',
         executable:'Node.js')

      task(type:'script',description:'micros service:deploy',
         scriptBody:'MICROS_TOKEN="$bamboo_micros_webcomponents_token_password" "$(npm bin)/micros" service:deploy "confluence-web-components" -f service/confluence-web-components.sd.yml -e "adev" -v')
   }

   environment(name:'Staging East') {
      notification(type:'Deployment Finished',recipient:'hipchat',apiKey:'${bamboo.atlassian.hipchat.apikey.password}',notify:'false',room:'Confluence Frontend')

      task(type:'addRequirement',description:'os equals linux') {
         requirement(key:'os',condition:'equals',value:'Linux')
      }
      task(type:'artifactDownload',description:'service-descriptor',
         planKey:'CONFNODE-WEBCOMPONENTS') {
         artifact(name:'service-descriptor',localPath:'service')
      }
      task(type:'npm',description:'install @atlassian/micros-cli --production',
         command:'install @atlassian/micros-cli --production',
         executable:'Node.js')

      task(type:'script',description:'micros service:deploy',
         scriptBody:'MICROS_TOKEN="$bamboo_micros_webcomponents_token_password" "$(npm bin)/micros" service:deploy "confluence-web-components" -f service/confluence-web-components.sd.yml -e "stg-east" -v')
   }

   environment(name:'Staging West') {
      notification(type:'Deployment Finished',recipient:'hipchat',apiKey:'${bamboo.atlassian.hipchat.apikey.password}',notify:'false',room:'Confluence Frontend')

      task(type:'addRequirement',description:'os equals linux') {
         requirement(key:'os',condition:'equals',value:'Linux')
      }
      task(type:'artifactDownload',description:'service-descriptor',
         planKey:'CONFNODE-WEBCOMPONENTS') {
         artifact(name:'service-descriptor',localPath:'service')
      }
      task(type:'npm',description:'install @atlassian/micros-cli --production',
         command:'install @atlassian/micros-cli --production',
         executable:'Node.js')

      task(type:'script',description:'micros service:deploy',
         scriptBody:'MICROS_TOKEN="$bamboo_micros_webcomponents_token_password" "$(npm bin)/micros" service:deploy "confluence-web-components" -f service/confluence-web-components.sd.yml -e "stg-west" -v')
   }

   environment(name:'Production East') {
      notification(type:'Deployment Finished',recipient:'hipchat',apiKey:'${bamboo.atlassian.hipchat.apikey.password}',notify:'false',room:'Confluence Frontend')

      task(type:'addRequirement',description:'os equals linux') {
         requirement(key:'os',condition:'equals',value:'Linux')
      }
      task(type:'artifactDownload',description:'service-descriptor',
         planKey:'CONFNODE-WEBCOMPONENTS') {
         artifact(name:'service-descriptor',localPath:'service')
      }
      task(type:'npm',description:'install @atlassian/micros-cli --production',
         command:'install @atlassian/micros-cli --production',
         executable:'Node.js')

      task(type:'script',description:'micros service:deploy',
         scriptBody:'MICROS_TOKEN="$bamboo_micros_webcomponents_token_password" "$(npm bin)/micros" service:deploy "confluence-web-components" -f service/confluence-web-components.sd.yml -e "prod-east" -v')
   }

   environment(name:'Production West') {
      notification(type:'Deployment Finished',recipient:'hipchat',apiKey:'${bamboo.atlassian.hipchat.apikey.password}',notify:'false',room:'Confluence Frontend')

      task(type:'addRequirement',description:'os equals linux') {
         requirement(key:'os',condition:'equals',value:'Linux')
      }
      task(type:'artifactDownload',description:'service-descriptor',
         planKey:'CONFNODE-WEBCOMPONENTS') {
         artifact(name:'service-descriptor',localPath:'service')
      }
      task(type:'npm',description:'install @atlassian/micros-cli --production',
         command:'install @atlassian/micros-cli --production',
         executable:'Node.js')

      task(type:'script',description:'micros service:deploy',
         scriptBody:'MICROS_TOKEN="$bamboo_micros_webcomponents_token_password" "$(npm bin)/micros" service:deploy "confluence-web-components" -f service/confluence-web-components.sd.yml -e "prod-west" -v')
   }

   permissions() {
      user(name:'dwalker',permissions:'read,write')
      anonymous(permissions:'read')
      loggedInUser(permissions:'read')
   }
}
