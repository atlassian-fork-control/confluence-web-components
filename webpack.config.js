var webpack = require('webpack');
var path = require('path');

var WebpackIsomorphicTools = require('webpack-isomorphic-tools');
var WebpackIsomorphicToolsPlugin = require('webpack-isomorphic-tools/plugin');
var webpackIsomorphicToolsPlugin = new WebpackIsomorphicToolsPlugin(require('./webpack-isomorphic-tools-configuration'));

var plugins = [];
if (process.env.NODE_ENV === 'production') {
    plugins = [
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ];
}

var entry = './src/index.js';
var moduleLoaders = [
    {
        test: /\.js?$/,
        exclude: /(node_modules)/,
        loader: 'babel'
    },
    {
        test: webpackIsomorphicToolsPlugin.regular_expression('svg'),
        loader: "url?limit=10240&mimetype=image/svg+xml"
    }
];

var defaultConfig = {
    entry: entry,
    output: {
        filename: process.env.NODE_ENV === 'production' ? './dist/confluence-web-components.min.js' : './dist/confluence-web-components.js'
    },
    module: {
        loaders: moduleLoaders
    },
    plugins: plugins,
    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    }
};

var flatpackConfig = {
    entry: entry,
    resolve: {
        alias: {
            'react': path.resolve('./node_modules/react')
        }
    },
    output: {
        filename: process.env.NODE_ENV === 'production' ? './dist/confluence-web-components-flatpack.min.js' : './dist/confluence-web-components-flatpack.js'
    },
    module: {
        loaders: moduleLoaders
    },
    plugins: plugins
};

module.exports = [defaultConfig, flatpackConfig];
