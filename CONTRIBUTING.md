## Starting the Connect plugin

NOTE: Confluence must be running and accessible from http://localhost:1990/confluence

    confluence-web-components$ npm install
    confluence-web-components$ npm start
    
## Re-building the component bundles

    confluence-web-components$ webpack -w

## Running the docker container locally

docker build -t docker.atlassian.io/atlassian/confluence-web-components .
docker run -t -p 3000:8080 -i docker.atlassian.io/atlassian/confluence-web-components

## Deploying updates to Micros

docker build -t docker.atlassian.io/atlassian/confluence-web-components .
docker push docker.atlassian.io/atlassian/confluence-web-components:latest
micros service:deploy confluence-web-components

