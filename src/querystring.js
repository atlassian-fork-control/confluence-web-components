export function parse(queryString) {
    if (queryString.charAt(0) === '?') {
        queryString = queryString.substr(1);
    }
    return queryString.split('&').reduce((prev, next) => {
        const parts = next.split('=');
        prev[parts[0]] = unescape(parts[1]);
        return prev;
    }, {});
}
