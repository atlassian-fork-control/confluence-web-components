import React from 'react';
import QuickSearch from '../QuickSearch';

import { shallow, render } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';

chai.use(chaiEnzyme());

import { spy } from 'sinon';
import sinonChai from 'sinon-chai';
chai.use(sinonChai);

describe('Quick search', () => {
    beforeEach(() => {
        global.document = {
            addEventListener : () => {}
        };
    });

    afterEach(() => {
        delete global.document;
    });

    it('should render an input field', () => {
        const wrapper = shallow(<QuickSearch />);
        expect(wrapper).to.have.descendants('input[type="text"]');
    });

    it('should render a search button', () => {
        const wrapper = shallow(<QuickSearch />);
        expect(wrapper).to.have.descendants('AUIButton');
    });

    it('should call the onFocusInput function when the input field is focused', () => {
        const focusSpy = spy();
        const wrapper = shallow(<QuickSearch onFocusInput={focusSpy} />);
        wrapper.find('input').simulate('focus');
        expect(focusSpy).to.have.callCount(1);
    });

    it('should attach a mouseup listener when the user is interacting ');

    describe('Recent pages', () => {
        it('should render a dropdown menu when there are recently visited pages and no search term has been provided', () => {
            const mockPages = [
                { id: 'ds', url: '', title: 'Welcome to Confluence', space: 'Demonstration Space'}
            ];
            const wrapper = shallow(<QuickSearch recentPages={mockPages} />);
            wrapper.find('input').simulate('focus');
            expect(wrapper).to.have.exactly(1).descendants('AUIDropdownMenu');
        });

        it('should render one link for each recently visited page', () => {
            const mockPages = [
                { id: 'ds', url: '', title: 'Welcome to Confluence', space: 'Demonstration Space'},
                { id: 'ds', url: '', title: 'Welcome to Confluence', space: 'Demonstration Space'}
            ];
            const wrapper = shallow(<QuickSearch recentPages={mockPages} />);
            wrapper.find('input').simulate('focus');
            expect(wrapper).to.have.exactly(2).descendants('AUIItemLink');
        });
    });

    describe('Getting context from result', () => {
        it('should return the correct context for pages', () => {
            const context = QuickSearch.prototype._getContext({
                id: 1234,
                className: 'content-type-page'
            });

            expect(context).to.deep.equal({ contentId: 1234 });
        });

        it('should return the correct context for blogposts', () => {
            const context = QuickSearch.prototype._getContext({
                id: 4321,
                className: 'content-type-blogpost'
            });

            expect(context).to.deep.equal({ contentId: 4321 });
        });

        it('should return the correct context for attachments', () => {
            const context = QuickSearch.prototype._getContext({
                id: 5555,
                href: 'http://localhost:1990/blah?preview=%2F9876',
                className: 'content-type-attachment-image'
            });

            expect(context).to.deep.equal({ contentId: 9876, attachmentId: 5555 });
        });

        it('should return the correct context for sitesearch', () => {
            const context = QuickSearch.prototype._getContext({
                className: 'search-for'
            });

            expect(context).to.deep.equal({ query: '' });
        });

        it('should return the correct context for user profile', () => {
            const context = QuickSearch.prototype._getContext({
                name: 'dwalker',
                className: 'content-type-userinfo'
            });

            expect(context).to.deep.equal({ username: 'dwalker' });
        });
    });
});
