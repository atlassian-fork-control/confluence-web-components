import React, { Component, PropTypes } from 'react';
import { xdm_e, cp } from '../context';

export default class CommentList extends Component {
    _comment({ comment, itemClassName, commentClassName, avatarContainerClassName, commentBodyClassName }) {
        return (
            <this.props.itemTagName className={itemClassName} key={comment.id}>
                <this.props.commentTagName className={commentClassName}>
                    <this.props.avatarContainerTagName className={avatarContainerClassName}>
                        <img className="avatar" src={`${xdm_e()}${comment.version.by.profilePicture.path}`} />
                    </this.props.avatarContainerTagName>
                    <strong><a href={`${xdm_e()}${cp()}/display/~${comment.version.by.username}`} target="_top">{comment.version.by.displayName}</a></strong>
                    <this.props.commentBodyTagName className={commentBodyClassName} dangerouslySetInnerHTML={{ __html: comment.body.export_view.value }} />
                    {
                        comment.children && comment.children.comment.results.length > 0 ? (
                            <this.props.containerTagName>
                                {comment.children.comment.results.map((childComment => this._comment({ comment: childComment, ...this.props })))}
                            </this.props.containerTagName>
                        ) : null
                    }
                </this.props.commentTagName>
            </this.props.itemTagName>
        );
    }
    render() {
        const { comments, containerClassName } = this.props;
        return (
            <this.props.containerTagName className={containerClassName}>
                {comments.map(comment => this._comment({ comment, ...this.props }))}
            </this.props.containerTagName>
        );
    }
}

CommentList.defaultProps = {
    containerTagName: 'ol',
    containerClassName: '',
    itemTagName: 'li',
    itemClassName: '',
    commentTagName: 'div',
    commentClassName: '',
    avatarContainerTagName: 'div',
    avatarContainerClassName: '',
    commentBodyTagName: 'div',
    commentBodyClassName: ''
};

CommentList.propTypes = {
    comments: PropTypes.array,
    containerTagName: PropTypes.string,
    containerClassName: PropTypes.string,
    itemTagName: PropTypes.string,
    itemClassName: PropTypes.string,
    commentTagName: PropTypes.string,
    commentClassName: PropTypes.string,
    avatarContainerTagName: PropTypes.string,
    avatarContainerClassName: PropTypes.string,
    commentBodyTagName: PropTypes.string,
    commentBodyClassName: PropTypes.string
};
