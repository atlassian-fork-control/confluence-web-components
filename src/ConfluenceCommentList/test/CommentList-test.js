import React from 'react';

import sinon, { spy, stub } from 'sinon';
import sinonChai from 'sinon-chai';
import { shallow, render } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import proxyquire from 'proxyquire';

chai.use(chaiEnzyme());
chai.use(sinonChai);

const CommentList = proxyquire('../CommentList', {
    '../context': {
        xdm_e: () => {},
        cp: () => {}
    }
}).default;

describe('ContentList', () => {
    it('should render an ol by default', () => {
        const mockContent = [];
        const wrapper = shallow(<CommentList comments={mockContent} />);
        expect(wrapper).to.have.descendants('ol');
    });

    it('should render a ol when ol is provided as the tag name', () => {
        const mockContent = [];
        const wrapper = shallow(<CommentList comments={mockContent} containerTagName="ol" />);
        expect(wrapper).to.have.descendants('ol');
    });

    it('should render a ul when ul is provided as the tag name', () => {
        const mockContent = [];
        const wrapper = shallow(<CommentList comments={mockContent} containerTagName="ul" />);
        expect(wrapper).to.have.descendants('ul');
    });

    it('should not render an li if there are not any comments', () => {
        const mockContent = [];
        const wrapper = shallow(<CommentList comments={mockContent} />);
        expect(wrapper).to.not.have.descendants('li');
    });

    it('should render a single li if there is 1 comment', () => {
        const mockContent = [
            { id : 1, _links: {}, version: { by: { profilePicture: {} } }, body: { export_view: {} } }
        ];
        const wrapper = shallow(<CommentList comments={mockContent} />);
        expect(wrapper).to.have.exactly(1).descendants('li');
    });

    it('should render an li for every comment', () => {
        const mockContent = [
            { id : 1, _links: {}, version: { by: { profilePicture: {} } }, body: { export_view: {} } },
            { id : 2, _links: {}, version: { by: { profilePicture: {} } }, body: { export_view: {} } },
            { id : 3, _links: {}, version: { by: { profilePicture: {} } }, body: { export_view: {} } },
            { id : 4, _links: {}, version: { by: { profilePicture: {} } }, body: { export_view: {} } }
        ];
        const wrapper = shallow(<CommentList comments={mockContent} />);
        expect(wrapper).to.have.exactly(4).descendants('li');
    });
});
