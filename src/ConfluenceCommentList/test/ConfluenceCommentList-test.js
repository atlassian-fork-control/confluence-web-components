import React from 'react';

import sinon, { spy, stub } from 'sinon';
import sinonChai from 'sinon-chai';
import { shallow, render } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import proxyquire from 'proxyquire';

chai.use(chaiEnzyme());
chai.use(sinonChai);

import ConfluenceCommentList from '../ConfluenceCommentList';

const navigatorSpy = {
    go: spy()
};

const mockResponse = '{ "results": [ { "foo": "bar" } ] }';

const mockAP = {
    request: (args) => {
        args.success(mockResponse);
    }
};

sinon.spy(mockAP, 'request');

const requireMock = (moduleName, callback) => {
    switch(moduleName) {
        case 'navigator':
            return callback(navigatorSpy);
            break;
        case 'request':
            return callback(mockAP.request);
            break;
    }
};

describe('ConfluenceCommentList', () => {
    beforeEach(() => {
        global.AP = {
            require: requireMock
        };
        sinon.spy(global.AP, 'require');
    });

    afterEach(() => {
        global.AP.require.restore();
        navigatorSpy.go.reset();
        mockAP.request.reset();
    });

    describe('Rendering', () => {
        it('should render an ol', () => {
            const wrapper = render(<ConfluenceCommentList />);
            expect(wrapper).to.have.descendants('ol');
        });
    });

    describe('When component mounts', () => {
        it('should request the comments if a content id is provided', () => {
            const mockThis = {
                _getComments: spy(),
                props: {
                    contentId: '1234'
                }
            };
            ConfluenceCommentList.prototype.componentDidMount.call(mockThis);
            expect(mockThis._getComments).to.have.callCount(1);
            expect(mockThis._getComments).to.have.been.calledWith(mockThis.props.contentId);
        });

        it('should not request the comments if content id is not provided', () => {
            const mockThis = {
                _getComments: spy(),
                props: {}
            };
            ConfluenceCommentList.prototype.componentDidMount.call(mockThis);
            expect(mockThis._getComments).to.have.callCount(0);
        });
    });

    describe('When component will receive properties', () => {
        it('should request the comments if the content id has changed', () => {
            const mockThis = {
                _getComments: spy(),
                props: {
                    contentId: '1234'
                }
            };
            ConfluenceCommentList.prototype.componentDidMount.call(mockThis);
            ConfluenceCommentList.prototype.componentWillReceiveProps.call(mockThis, { contentId: '4321' });
            expect(mockThis._getComments).to.have.callCount(2);
            expect(mockThis._getComments).to.have.been.calledWith(mockThis.props.contentId);
            expect(mockThis._getComments).to.have.been.calledWith('4321');
        });

        it('should not request the comments if the content id is the same', () => {
            const mockThis = {
                _getComments: spy(),
                props: {
                    contentId: '1234'
                }
            };
            ConfluenceCommentList.prototype.componentDidMount.call(mockThis);
            ConfluenceCommentList.prototype.componentWillReceiveProps.call(mockThis, { contentId: '1234' });
            expect(mockThis._getComments).to.have.callCount(1);
            expect(mockThis._getComments).to.have.been.calledWith(mockThis.props.contentId);
        });
    });

    describe('When getting comments', () => {
        it('should call the correct URL', () => {
            const mockThis = {
                setState: () => {},
                _getExpansions: () => { return 'body.export_view,version'; },
                props: {
                    depth: 1
                }
            };
            ConfluenceCommentList.prototype._getComments.call(mockThis, '1234');
            expect(mockAP.request).to.have.callCount(1);
            expect(mockAP.request.firstCall.args[0].url).to.equal('/rest/api/content/1234/child/comment?expand=body.export_view,version');
        });

        it('should assign the response results to the component state', () => {
            const mockThis = {
                setState: spy(),
                _getExpansions: () => {},
                props: {
                    depth: 1
                }
            };
            ConfluenceCommentList.prototype._getComments.call(mockThis, '1234');
            expect(mockThis.setState).to.have.been.calledWith({ commentList: [ { 'foo' : 'bar' }]});
        });
    });

    describe('Rest API expansions', () => {
        it('should return the correct expansions for a depth of 1', () => {
            expect(ConfluenceCommentList.prototype._getExpansions(1)).to.equal('body.export_view,version');
        });

        it('should return the correct expansions for a depth of 2', () => {
            expect(ConfluenceCommentList.prototype._getExpansions(2)).to.equal('body.export_view,version,children.comment.body.export_view,children.comment.version');
        });

        it('should return the correct expansions for a depth of 3', () => {
            expect(ConfluenceCommentList.prototype._getExpansions(3)).to.equal('body.export_view,version,children.comment.body.export_view,children.comment.version,children.comment.children.comment.body.export_view,children.comment.children.comment.version');
        });
    });
});
