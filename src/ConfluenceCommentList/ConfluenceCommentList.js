import React, { Component, PropTypes } from 'react';
import CommentList from './CommentList';

export default class ConfluenceCommentList extends Component {
    constructor() {
        super();
        this.state = {
            commentList: []
        };
    }
    componentDidMount() {
        if (this.props.contentId) {
            this._getComments(this.props.contentId);
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.contentId !== this.props.contentId) {
            this._getComments(nextProps.contentId);
        }
    }
    _getExpansions(depth) {
        const defaultExpansions = ['body.export_view','version'];
        const baseChildren = 'children.comment';
        if (depth === 1) {
            return defaultExpansions.join(',');
        } else {
            let childExpansions = [];
            const childPath = [];
            for(let i = 1; i < depth; i++) {
                childPath.push(baseChildren);
                childExpansions = childExpansions.concat(defaultExpansions.map(expansion => {
                    return `${childPath.join('.')}.${expansion}`;
                }));
            }

            return [...defaultExpansions, ...childExpansions].join(',');
        }
    }
    _getComments(contentId) {
        AP.require('request', request => {
            request({
                url: `/rest/api/content/${contentId}/child/comment?expand=${this._getExpansions(this.props.depth)}`,
                success: response => {
                    response = JSON.parse(response);
                    this.setState({
                        commentList: response.results
                    });
                }
            });
        });
    }
    render() {
        return (
            <CommentList
                comments={this.state.commentList}
                containerTagName={this.props.containerTagName}
                containerClassName={this.props.containerClassName}
                itemTagName={this.props.itemTagName}
                itemClassName={this.props.itemClassName}
                commentTagName={this.props.commentTagName}
                commentClassName={this.props.commentClassName}
                avatarContainerTagName={this.props.avatarContainerTagName}
                avatarContainerClassName={this.props.avatarContainerClassName}
            />
        );
    }
}

ConfluenceCommentList.defaultProps = {
    depth: '1',
    containerTagName: 'ol',
    containerClassName: '',
    itemTagName: 'li',
    itemClassName: '',
    commentTagName: 'div',
    commentClassName: '',
    avatarContainerTagName: 'div',
    avatarContainerClassName: '',
    commentBodyTagName: 'div',
    commentBodyClassName: ''
};

ConfluenceCommentList.propTypes = {
    depth: PropTypes.string,
    contentId: PropTypes.string,
    containerTagName: PropTypes.string,
    itemTagName: PropTypes.string,
    itemClassName: PropTypes.string,
    containerClassName: PropTypes.string,
    commentTagName: PropTypes.string,
    commentClassName: PropTypes.string,
    avatarContainerTagName: PropTypes.string,
    avatarContainerClassName: PropTypes.string,
    commentBodyTagName: PropTypes.string,
    commentBodyClassName: PropTypes.string
};
