import React from 'react';

import sinon, { spy, stub } from 'sinon';
import sinonChai from 'sinon-chai';
import { shallow, render } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import proxyquire from 'proxyquire';

chai.use(chaiEnzyme());
chai.use(sinonChai);

const contextStub = stub().returns('http://localhost:0000/');

const navigatorSpy = {
    go: spy()
};

const mockResponse = '{ "name" : "Demonstration Space" }';

const mockAP = {
    request: (args) => {
        args.success(mockResponse);
    }
};

sinon.spy(mockAP, 'request');

const requireMock = (moduleName, callback) => {
    switch(moduleName) {
        case 'navigator':
            return callback(navigatorSpy);
            break;
        case 'request':
            return callback(mockAP.request);
            break;
    }
};

const ConfluenceSpaceTitle = proxyquire('../ConfluenceSpaceTitle', {
    '../context': {
        xdm_e: contextStub
    }
}).default;

describe('ConfluenceSpaceTitle', () => {
    beforeEach(() => {
        global.AP = {
            require: requireMock
        };
        sinon.spy(global.AP, 'require');
        contextStub.reset();
    });

    afterEach(() => {
        global.AP.require.restore();
        navigatorSpy.go.reset();
        mockAP.request.reset();
    });

    describe('Rendering', () => {
        it('should render an h2', () => {
            const wrapper = render(<ConfluenceSpaceTitle />);
            expect(wrapper).to.have.descendants('h2');
        });
    });

    describe('When component mounts', () => {
        it('should request the space title if a space key is provided', () => {
            const mockThis = {
                _getSpaceTitle: spy(),
                props: {
                    spaceKey: 'ds'
                }
            };
            ConfluenceSpaceTitle.prototype.componentDidMount.call(mockThis);
            expect(mockThis._getSpaceTitle).to.have.callCount(1);
            expect(mockThis._getSpaceTitle).to.have.been.calledWith(mockThis.props.spaceKey);
        });

        it('should not request the space title if space key is not provided', () => {
            const mockThis = {
                _getSpaceTitle: spy(),
                props: {}
            };
            ConfluenceSpaceTitle.prototype.componentDidMount.call(mockThis);
            expect(mockThis._getSpaceTitle).to.have.callCount(0);
        });
    });

    describe('When component will receive properties', () => {
        it('should request the space title if the space key has changed', () => {
            const mockThis = {
                _getSpaceTitle: spy(),
                props: {
                    spaceKey: 'ds'
                }
            };
            ConfluenceSpaceTitle.prototype.componentDidMount.call(mockThis);
            ConfluenceSpaceTitle.prototype.componentWillReceiveProps.call(mockThis, { spaceKey: 'sd' });
            expect(mockThis._getSpaceTitle).to.have.callCount(2);
            expect(mockThis._getSpaceTitle).to.have.been.calledWith(mockThis.props.spaceKey);
            expect(mockThis._getSpaceTitle).to.have.been.calledWith('sd');
        });

        it('should not request space title if the space key is the same', () => {
            const mockThis = {
                _getSpaceTitle: spy(),
                props: {
                    spaceKey: 'ds'
                }
            };
            ConfluenceSpaceTitle.prototype.componentDidMount.call(mockThis);
            ConfluenceSpaceTitle.prototype.componentWillReceiveProps.call(mockThis, { spaceKey: 'ds' });
            expect(mockThis._getSpaceTitle).to.have.callCount(1);
            expect(mockThis._getSpaceTitle).to.have.been.calledWith(mockThis.props.spaceKey);
        });
    });

    describe('When getting space title', () => {
        it('should call the correct URL', () => {
            const mockThis = {
                setState: () => {}
            };
            ConfluenceSpaceTitle.prototype._getSpaceTitle.call(mockThis, 'ds');
            expect(mockAP.request).to.have.callCount(1);
            expect(mockAP.request.firstCall.args[0].url).to.equal('/rest/api/space/ds');
        });

        it('should assign the content body to the component state', () => {
            const mockThis = {
                setState: spy()
            };
            ConfluenceSpaceTitle.prototype._getSpaceTitle.call(mockThis, 'ds');
            expect(mockThis.setState).to.have.been.calledWith({
                spaceTitle: 'Demonstration Space'
            });
        });
    });
});
