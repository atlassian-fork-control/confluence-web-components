import React, { Component, PropTypes } from 'react';

export default class ConfluenceSpaceTitle extends Component {
    constructor() {
        super();
        this.state = {
            spaceTitle: ''
        };
    }
    componentDidMount() {
        if (this.props.spaceKey) {
            this._getSpaceTitle(this.props.spaceKey);
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.spaceKey !== this.props.spaceKey) {
            this._getSpaceTitle(nextProps.spaceKey);
        }
    }
    _getSpaceTitle(spaceKey) {
        AP.require('request', request => {
            request({
                url: `/rest/api/space/${spaceKey}`,
                success: response => {
                    this.setState({ spaceTitle: JSON.parse(response).name });
                }
            });
        });
    }
    render() {
        return <this.props.tagName>{this.state.spaceTitle}</this.props.tagName>;
    }
}

ConfluenceSpaceTitle.defaultProps = {
    tagName: 'h2'
};

ConfluenceSpaceTitle.propTypes = {
    spaceKey: PropTypes.string
};
