import React, { Component, PropTypes } from 'react';

import SpaceList from './SpaceList';

export default class ConfluenceSpaceList extends Component {
    constructor() {
        super();
        this.state = {
            content: []
        };
    }
    componentDidMount() {
        this._getSpaces();
    }
    componentWillReceiveProps(nextProps) {
        this._getSpaces();
    }
    _getSpaces() {
        AP.require('request', request => {
            request({
                url: `/rest/api/space?limit=${this.props.limit}`,
                success: response => {
                    response = JSON.parse(response);
                    this.setState({
                        baseUrl: response._links.base,
                        contextPath: response._links.context,
                        content: response.results
                    });
                }
            });
        });
    }
    render() {
        return this.state.content ? (
            <SpaceList
                baseUrl={this.state.baseUrl}
                content={this.state.content}
                tagName={this.props.tagName}
                containerClass={this.props.containerClass}
                itemClass={this.props.itemClass}
            />
        ) : null;
    }
}

ConfluenceSpaceList.defaultProps = {
    tagName: 'ol',
    limit: '25'
};

ConfluenceSpaceList.propTypes = {
    tagName: PropTypes.string,
    containerClass: PropTypes.string,
    itemClass: PropTypes.string,
    limit: PropTypes.string
};
