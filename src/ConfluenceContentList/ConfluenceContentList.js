import React, { Component, PropTypes } from 'react';

import ContentList from './ContentList';

export default class ConfluenceContentList extends Component {
    constructor() {
        super();
        //this._handleClick = this._handleClick.bind(this);
        this.state = {
            content: []
        };
    }
    componentDidMount() {
        if (this.props.cql) {
            this._getContent(this.props.cql);
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.cql !== this.props.cql) {
            this._getContent(nextProps.cql);
        }
    }
    _getContent(cql) {
        AP.require('request', request => {
            request({
                url: `/rest/api/search?cql=${cql}&limit=${this.props.limit}`,
                success: response => {
                    response = JSON.parse(response);
                    this.setState({
                        baseUrl: response._links.base,
                        contextPath: response._links.context,
                        content: response.results
                    });
                }
            });
        });
    }
    //componentDidUpdate() {
    //    const anchors = this.refs.container.querySelectorAll('a');
    //    for(let i = 0, anchor; anchor = anchors[i]; i++) {
    //        anchor.addEventListener('click', this._navigate, false);
    //    }
    //}
    //_navigate(event) {
    //    const contentId = this.href.match(/\?pageId=([0-9]+)/);
    //    if (contentId) {
    //        AP.require('navigator', navigator => {
    //            navigator.go('contentview', { contentId: contentId[1] });
    //        });
    //    } else {
    //        console.error('Could not determine navigation context for ' + this.href);
    //    }
    //    event.preventDefault();
    //}
    //_handleChange(event) {
    //    console.log(event);
    //    const changeEvent = new CustomEvent('change', { bubbles: true });
    //    //React.findDOMNode(this).dispatchEvent(changeEvent);
    //    //console.log(this.refs.container);
    //    this.refs.container.dispatchEvent(changeEvent);
    //}
    //_handleClick(contentId, event) {
        //const navigateEvent = new CustomEvent('navigate', {
        //    detail: {
        //        contentId
        //    },
        //    bubbles: true
        //});
        //ReactDOM.findDOMNode(this).dispatchEvent(navigateEvent);
        //event.preventDefault();
    //}
    // onClick={this._handleClick.bind(this, result.id)}
    render() {
        return (
            <ContentList
                baseUrl={this.state.baseUrl}
                content={this.state.content}
                tagName={this.props.tagName}
                containerClass={this.props.containerClass}
                itemClass={this.props.itemClass}
            />
        );
    }
}

ConfluenceContentList.defaultProps = {
    tagName: 'ol',
    limit: '25'
};

ConfluenceContentList.propTypes = {
    tagName: PropTypes.string,
    containerClass: PropTypes.string,
    itemClass: PropTypes.string,
    id: PropTypes.string,
    cql: PropTypes.string,
    limit: PropTypes.string
};
