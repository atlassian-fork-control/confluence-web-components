import React from 'react';

import sinon, { spy, stub } from 'sinon';
import sinonChai from 'sinon-chai';
import { shallow, render } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import proxyquire from 'proxyquire';

chai.use(chaiEnzyme());
chai.use(sinonChai);

import ContentList from '../ContentList';

describe('ContentList', () => {
    it('should render an ol by default', () => {
        const mockContent = [];
        const wrapper = shallow(<ContentList content={mockContent} />);
        expect(wrapper).to.have.descendants('ol');
    });

    it('should render a ol when ol is provided as the tag name', () => {
        const mockContent = [];
        const wrapper = shallow(<ContentList content={mockContent} tagName="ol" />);
        expect(wrapper).to.have.descendants('ol');
    });

    it('should render a ul when ul is provided as the tag name', () => {
        const mockContent = [];
        const wrapper = shallow(<ContentList content={mockContent} tagName="ul" />);
        expect(wrapper).to.have.descendants('ul');
    });

    it('should not render an li if there are not content item', () => {
        const mockContent = [];
        const wrapper = shallow(<ContentList content={mockContent} />);
        expect(wrapper).to.not.have.descendants('li');
    });

    it('should render a single li if there is 1 content item', () => {
        const mockContent = [
            { content: { id : 1, _links: {} } }
        ];
        const wrapper = shallow(<ContentList content={mockContent} />);
        expect(wrapper).to.have.exactly(1).descendants('li');
    });

    it('should render an li for every content item', () => {
        const mockContent = [
            { content: { id : 1, _links: {} } },
            { content: { id : 2, _links: {} } },
            { content: { id : 3, _links: {} } },
            { content: { id : 4, _links: {} } }
        ];
        const wrapper = shallow(<ContentList content={mockContent} />);
        expect(wrapper).to.have.exactly(4).descendants('li');
    });
});
