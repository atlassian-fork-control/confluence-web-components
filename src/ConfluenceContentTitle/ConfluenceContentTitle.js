import React, { Component, PropTypes } from 'react';

export default class ConfluenceContentTitle extends Component {
    constructor() {
        super();
        this.state = {
            contentTitle: ''
        };
    }
    componentDidMount() {
        if (this.props.contentId) {
            this._getContentTitle(this.props.contentId);
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.contentId !== this.props.contentId) {
            this._getContentTitle(nextProps.contentId);
        }
    }
    _getContentTitle(contentId) {
        AP.require('request', request => {
            request({
                url: `/rest/api/content/${contentId}`,
                success: response => {
                    const content = JSON.parse(response);
                    this.setState({
                        contentTitle: content.title,
                        contentLink: `${content._links.base}${content._links.webui}`
                    });
                }
            });
        });
    }
    render() {
        return (
            <this.props.tagName className={this.props.className}>
                {this.props.linkToContent ? <a href={this.state.contentLink} target="_top">{this.state.contentTitle}</a> : this.state.contentTitle}
            </this.props.tagName>
        );
    }
}

ConfluenceContentTitle.defaultProps = {
    tagName: 'h2'
};

ConfluenceContentTitle.propTypes = {
    className: PropTypes.string,
    linkToContent: PropTypes.string,
    tagName: PropTypes.string,
    contentId: PropTypes.string
};
