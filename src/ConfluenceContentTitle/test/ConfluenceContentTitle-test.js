import React from 'react';

import sinon, { spy, stub } from 'sinon';
import sinonChai from 'sinon-chai';
import { shallow, render } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import proxyquire from 'proxyquire';

import { renderToStaticMarkup } from 'react-dom/server';

chai.use(chaiEnzyme());
chai.use(sinonChai);

const contextStub = stub().returns('http://localhost:0000/');

const navigatorSpy = {
    go: spy()
};

const mockResponse = '{ "_links": { "base": "http://localhost:0000/wiki/", "webui": "ds/Welcome+to+Confluence" }, "title": "Welcome to Confluence" }';

const mockAP = {
    request: (args) => {
        args.success(mockResponse);
    }
};

sinon.spy(mockAP, 'request');

const requireMock = (moduleName, callback) => {
    switch(moduleName) {
        case 'navigator':
            return callback(navigatorSpy);
            break;
        case 'request':
            return callback(mockAP.request);
            break;
    }
};

const ConfluenceContentTitle = proxyquire('../ConfluenceContentTitle', {
    '../context': {
        xdm_e: contextStub
    }
}).default;

describe('ConfluenceContentTitle', () => {
    beforeEach(() => {
        global.AP = {
            require: requireMock
        };
        sinon.spy(global.AP, 'require');
        contextStub.reset();
    });

    afterEach(() => {
        global.AP.require.restore();
        navigatorSpy.go.reset();
        mockAP.request.reset();
    });

    describe('Rendering', () => {
        it('should render an h2', () => {
            expect(renderToStaticMarkup(<ConfluenceContentTitle />)).to.equal('<h2></h2>');
        });

        it('should render an anchor when linkToContent is true', () => {
            expect(renderToStaticMarkup(<ConfluenceContentTitle linkToContent />)).to.equal('<h2><a target="_top"></a></h2>');
        });
    });

    describe('When component mounts', () => {
        it('should request the content title if a content id is provided', () => {
            const mockThis = {
                _getContentTitle: spy(),
                props: {
                    contentId: '1234'
                }
            };
            ConfluenceContentTitle.prototype.componentDidMount.call(mockThis);
            expect(mockThis._getContentTitle).to.have.callCount(1);
            expect(mockThis._getContentTitle).to.have.been.calledWith(mockThis.props.contentId);
        });

        it('should not request the content title if content id is not provided', () => {
            const mockThis = {
                _getContentTitle: spy(),
                props: {}
            };
            ConfluenceContentTitle.prototype.componentDidMount.call(mockThis);
            expect(mockThis._getContentTitle).to.have.callCount(0);
        });
    });

    describe('When component will receive properties', () => {
        it('should request the content title if the content id has changed', () => {
            const mockThis = {
                _getContentTitle: spy(),
                props: {
                    contentId: '1234'
                }
            };
            ConfluenceContentTitle.prototype.componentDidMount.call(mockThis);
            ConfluenceContentTitle.prototype.componentWillReceiveProps.call(mockThis, { contentId: '4321' });
            expect(mockThis._getContentTitle).to.have.callCount(2);
            expect(mockThis._getContentTitle).to.have.been.calledWith(mockThis.props.contentId);
            expect(mockThis._getContentTitle).to.have.been.calledWith('4321');
        });

        it('should not request content title if the content id is the same', () => {
            const mockThis = {
                _getContentTitle: spy(),
                props: {
                    contentId: '1234'
                }
            };
            ConfluenceContentTitle.prototype.componentDidMount.call(mockThis);
            ConfluenceContentTitle.prototype.componentWillReceiveProps.call(mockThis, { contentId: '1234' });
            expect(mockThis._getContentTitle).to.have.callCount(1);
            expect(mockThis._getContentTitle).to.have.been.calledWith(mockThis.props.contentId);
        });
    });

    describe('When getting content title', () => {
        it('should call the correct URL', () => {
            const mockThis = {
                baseUrl: 'http://localhost:1990/',
                props: {
                    limit: 25
                },
                setState: () => {}
            };
            ConfluenceContentTitle.prototype._getContentTitle.call(mockThis, '1234');
            expect(mockAP.request).to.have.callCount(1);
            expect(mockAP.request.firstCall.args[0].url).to.equal('/rest/api/content/1234');
        });

        it('should assign the content body to the component state', () => {
            const mockThis = {
                baseUrl: 'http://localhost:1990/',
                props: {
                    limit: 25
                },
                setState: spy()
            };
            ConfluenceContentTitle.prototype._getContentTitle.call(mockThis, 'type=space');
            expect(mockThis.setState).to.have.been.calledWith({
                contentTitle: 'Welcome to Confluence',
                contentLink: 'http://localhost:0000/wiki/ds/Welcome+to+Confluence'
            });
        });
    });
});
