import 'document-register-element';
import 'reactive-elements';

import ConfluenceQuickSearch from './ConfluenceQuickSearch';
document.registerReact('confluence-quick-search', ConfluenceQuickSearch);

import ConfluenceCommentList from './ConfluenceCommentList';
document.registerReact('confluence-comment-list', ConfluenceCommentList);

import ConfluenceContentBody from './ConfluenceContentBody';
document.registerReact('confluence-content-body', ConfluenceContentBody);

import ConfluenceContentList from './ConfluenceContentList';
document.registerReact('confluence-content-list', ConfluenceContentList);

import ConfluenceSpaceList from './ConfluenceSpaceList';
document.registerReact('confluence-space-list', ConfluenceSpaceList);

import ConfluenceSpaceTitle from './ConfluenceSpaceTitle';
document.registerReact('confluence-space-title', ConfluenceSpaceTitle);

import ConfluenceContentTitle from './ConfluenceContentTitle';
document.registerReact('confluence-content-title', ConfluenceContentTitle);
