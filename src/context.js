import { parse } from './querystring';

export function cp() {
    return parse(window.location.search)['cp'];
}

export function xdm_e() {
    return parse(window.location.search)['xdm_e'];
}
