import { parse } from '../src/querystring';
import { expect } from 'chai';

describe('Querystring module', () => {
    describe('Parsing behaviour', () => {
        it('should correctly extract parameters from a connect context URL', () => {
            const context = parse('?tz=Australia%2FSydney&loc=en-GB&user_id=admin&xdm_e=http%3A%2F%2Flocalhost%3A1990&cp=%2Fconfluence');

            expect(context.tz).to.equal('Australia/Sydney');
            expect(context.loc).to.equal('en-GB');
            expect(context.user_id).to.equal('admin');
            expect(context.xdm_e).to.equal('http://localhost:1990');
            expect(context.cp).to.equal('/confluence');
        });

        it('should handle string without a leading ?', () => {
            const context = parse('tz=Australia%2FSydney&loc=en-GB&user_id=admin&xdm_e=http%3A%2F%2Flocalhost%3A1990&cp=%2Fconfluence');

            expect(context.tz).to.equal('Australia/Sydney');
            expect(context.loc).to.equal('en-GB');
            expect(context.user_id).to.equal('admin');
            expect(context.xdm_e).to.equal('http://localhost:1990');
            expect(context.cp).to.equal('/confluence');
        });
    });
});
