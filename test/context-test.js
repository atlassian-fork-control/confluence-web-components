import { expect } from 'chai';

import { xdm_e, cp } from '../src/context';

describe('Context module', () => {
    beforeEach(() => {
        global.window = {
            location:  { search: '?xdm_e=http://localhost:1990&cp=/confluence' }
        };
    });

    afterEach(() => {
        delete global.window;
    });

    it('should return the value of xdm_e from window.location.search', () => {
        expect(xdm_e()).to.equal('http://localhost:1990');
    });

    it('should return the value of cp from window.location.search', () => {
        expect(cp()).to.equal('/confluence');
    });
});
