#Deprecated
**This module will no longer be maintained and can be removed at anytime, use at your own risk**



## Overview
This is a library that compliments [Atlassian Connect Add-on](https://developer.atlassian.com/static/connect/docs/latest/guides/getting-started.html) Theme module. It aims to provide a set of web components to reproduce some of the functionality in Confluence in a Confluence add-on Theme.

## Installation

```
npm install confluence-web-components
```

## Usage

```
<!doctype html>
<head>
    <title>My connect add-on</title>
    <script src="node_modules/confluence-web-components/dist/confluence-web-components.js"></script>
</head>
<body>
    ...
    <confluence-quick-search placeholder="Search..."></confluence-quick-search>
    ...
</body>
</html>
```

## Bundles

The Confluence web components are provided as 4 different bundles, which contain all components:

File                                           | Size (gzipped) | Description
---------------------------------------------- | -------------- | ------------------------------------------------------
dist/confluence-web-components-flatpack.js     | 156kb          | This is the unminified bundle with all dependencies included. This is the best option for getting up and running and for use in your development environment.
dist/confluence-web-components-flatpack.min.js | 44kb           | The minified version of the bundle above. This is what you should use in production.
dist/confluence-web-components.js              | 11.4kb         | This is the unminified bundle with React and ReactDOM excluded. You should use this bundle if you use React for your own components and would like to use a shared/CDN copy. The bundle assumes that React and ReactDOM are available as global variables.
dist/confluence-web-components.min.js          | 6.6kb          | The minified version of the previous version. This is the best option for a production environment using a CDN copy of React and ReactDOM.

NOTE: It is not currently supported to consume individual web components or create your own custom bundle.

## Quick search

```
<confluence-quick-search
    container-class={string}
    placeholder={string}
></confluence-quick-search>
```

Allows users to easily navigate to recently visited pages, perform a search without leaving the current page, or performance a site-wide search.

**container-class {string}** - A CSS class name that will be applied to the root DOM node.  Use this to help target styles in your theme.
**placeholder {bool}** - Placeholder text to show in the search input field.

## Content body

```
<confluence-content-body
    content-id={number}
></confluence-content-body>
```

Renders the HTML content for a piece of content (e.g. page or blogpost). This does not include the content title or comments.

**content-id {number}** - The ID of the content to render. This parameter is required.

## Content list

Displays a list of content items based on a CQL query.

```
<confluence-content-list
    cql={string}
></confluence-content-list>
```

**cql {string}** - The CQL used to retrieve content.
**tag-name {ul|ol=ol}** - The tag of the containing element. Default is "ol".
**limit {number=25}** - The maximum number of items to fetch and display. Default is 25.
**container-class** - Will be applied to the root element.
**item-class** - Will be applied to each item in the list.

## Running the Connect add-on locally

### Setup this repo locally

```
git clone https://bitbucket.org/atlassian/confluence-web-components.git
nvm install v4.2.2             # switch to node 4.2.2
npm install                    # install dependencies
```

### Start Confluence on port 1990
### Start the add-on on port 3000

```
npm start
```

When the add-on starts, it will automatically register itself with the local Confluence instance.

## Potential roadmap

### v1.0
* Will include the following components:
    - Comment list
    - Content body
    - Content list
    - Content title
    - Quick search
    - Space title
    - Space list
* Will use React 0.14.5

### v1.0.x
* Will only include bug fixes or backwards compatible changes to components from 1.0 release.

### v1.x
* Will include new components, potential deprecation of existing components, or minor React version updates.